# remindme

This script uses python3 to send notification reminders in defined intervals. Supports MacOS and Linux.

### Installation
```bash
wget -O remindme https://gitlab.com/daviddiefenderfer/remindme/raw/master/remindme
chmod +x ./remindme
remindme 15m "Call my mom"
```

### Usage
```bash
remindme 15s "Do something" # Sends a notification in 15 seconds
remindme 15m "Do something" # Sends a notification in 15 minutes
remindme 15h "Do something" # Sends a notification in 15 hours
```
